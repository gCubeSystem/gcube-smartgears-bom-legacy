This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Smartgears Bom

# [v2.5.1]

- Enhanced gcube-bom to 2.4.1
- Added document-store-lib-accounting-service
- Enhanced authorization-client lower bound of range
- Enhanced common-authorization lower bound of range


# [v2.5.0]

- Upgraded gcube-bom to 2.4.0
- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range


# [v2.4.0]

- update gcube-bom to 2.3.0
- remove versions from components alredy defined in gcube-bom


# [v2.3.0]

- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3


## [v2.2.0]

- Enhanced gcube-model version range
- Enhanced information-system-model version range
- Upgraded gcube-bom version to 2.1.0-SNAPSHOT


## [v2.1.1]

- Enhanced gcube-bom version


## [v2.1.0]

- Added missing new IS libs

## [v2.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19283]


## [v1.2.0] [r4.21.0] - 2020-03-30

- Updated gcube-bom version


## [v1.1.0] [r4.17.0] - 2019-12-19

- Added version already declared in gcube-bom to avoid that some component does not inherits the version
- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.0.2] [r4.15.0] - 2019-11-08

- Fixed distro files and pom according to new release procedure


## [v1.0.1] [r4.13.1] - 2019-02-26

- Added common-smartgears-utils dependency
- Added gxHTTP dependency


## [v1.0.0] [r4.2.0] - 2016-12-15

- First Release

